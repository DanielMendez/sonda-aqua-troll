// Crear servidor en el puerto 80
AsyncWebServer server(80);

// Función para cambiar el estado del botón del HTML
String button(const String& var_html) {
  Serial.println(var_html);
  // Se verifica que el HTML haya pedido la variable STATE
  if (var_html == "STATE") {
    if (stateButton) {
      buttonState = "ON";
    }
    else {
      buttonState = "OFF";
    }
    Serial.println(buttonState);
    return buttonState;
  }
  return String();
}

void initServer() {
  // Creamos el punto de acceso
  WiFi.softAP(ssid, password);
  WiFi.softAPConfig(ip, gateway, subnet);

  // Configurar la URL raíz como la ruta raíz de la microSD
  server.serveStatic("/", SD, "/");

  // Respuestas para los eventos de petición que realice el cliente
  // Ingreso a la ruta raíz "/"
  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SD, "/index.html", String(), false, button);
  });
  // Pulsación del botón láseres (ingreso a la dirección /state)
  server.on("/state", HTTP_GET, [](AsyncWebServerRequest * request) {
    stateButton = !stateButton;
    request->send(SD, "/index.html", String(), false, button);
  });
  // Pulsación del botón descargar datos (ingreso a la dirección /state)
  server.on("/download", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SD, "/datos.csv", String(), true);
  });
  server.on("/get", HTTP_GET, [] (AsyncWebServerRequest * request) {
    String inputMessage;
    String inputParam;
    // GET input1 value on <ESP_IP>/get?input1=<inputMessage>
    if (request->hasParam(PARAM_INPUT_1)) {
      inputMessage = request->getParam(PARAM_INPUT_1)->value();
      inputParam = PARAM_INPUT_1;
    }
    else {
      inputMessage = "No message sent";
      inputParam = "none";
    }
    Serial.println(inputMessage);

    recivCommand(inputMessage);

    request->redirect("/");
  });


  // Iniciar servidor
  server.begin();
  Serial.println("Servidor HTTP iniciado");
  delay(150);
}
