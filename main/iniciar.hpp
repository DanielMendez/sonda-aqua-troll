/*
  Funcion de inicio (modificar de ser necesario)
*/

void initSys() {
  Serial.begin(115200);    //para el debugging
  
  //se comprueba que se haya iniciado la SD
  if (!SD.begin()) {
    Serial.println("No se encuentra la SD");
    return;
  }
  //iniciar sensor
  mySDI12.begin();
  delay(500);

  // Power the sensors;
  if (POWER_PIN > 0) {
    pinMode(POWER_PIN, OUTPUT);
    digitalWrite(POWER_PIN, HIGH);
    delay(200);
  }

  for (byte i = 0; i < 62; i++) {
    char addr = decToChar(i);
    if (checkActive(addr)) {
      numSensors++;
      isActive[i] = 1;
      //        printInfo(addr);
      //        Serial.println();
    }
  }
  //    Serial.print("Total number of sensors found:  ");
  //    Serial.println(numSensors);

  if (numSensors == 0) {
    //        Serial.println(
    //        "No sensors found, please check connections and restart the Arduino.");
    while (true) {
      delay(10);  // do nothing forever
    }
  }

  String header_txt = "Tiempo (s), CLO, OD, SOD, COE, SAL, DEN, STD, pH, ORP, TEM, PRE, PRO\n ";
  appendFile(SD, "/datos.csv", header_txt.c_str());
}
