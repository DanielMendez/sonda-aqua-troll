/*
 * Funciones auxiliares para trabajar con la SD
 * y el ESP32
 * --------------------------------------------
 * Coneccion:
 * SD Card | ESP32
 * ....................   
 * MISO       G19   
 * SCK        G18   
 * MOSI       G23   
 * CS         G5 
 *
 */



/* ------------------------------
    FUNCIONES AUXILIARES PARA LA TARJETA
  --------------------------------*/
void createDir(fs::FS &fs, const char * path) {
  //Serial.printf("Creating Dir: %s\n", path);
  fs.mkdir(path);
}


void writeFile(fs::FS &fs, const char * path, const char * message) {
  //Serial.printf("Writing file: %s\n", path);
  File file = fs.open(path, FILE_WRITE);
  file.print(message);
  //file.close();
}

void appendFile(fs::FS &fs, const char * path, const char * message) {
  //Serial.printf("Appending to file: %s\n", path);
  File file = fs.open(path, FILE_APPEND);
  file.print(message);
  //file.close();
}

void deleteFile(fs::FS &fs, const char * path){
    fs.remove(path);
}
