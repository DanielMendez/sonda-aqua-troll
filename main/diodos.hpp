/*
 * Funcion que mantiene el funcionamiento de los láser de forma intermitente
 * (10s encendido y 2s apagado)
 */
void inter_d1d2(unsigned long currentMillis){
  if (currentMillis-previousMillis >= 10000 && ledState == HIGH){
    ledState = LOW;
    digitalWrite(D1, ledState);
    digitalWrite(D2, ledState);
    previousMillis = currentMillis;
  }
  else if (currentMillis - previousMillis>=2000 && ledState == LOW){
    ledState = HIGH;
    digitalWrite(D1, ledState);
    digitalWrite(D2, ledState);
    previousMillis = currentMillis;
  }
}
